package org.trahomoto.props;

/**
 * Каропка с волшебной телкой
 */
public class MagicBoxImpl implements MagicBox {
    @Override
    public String getContents() {
        return "Сисястая тёлка";
    }
}
