package org.trahomoto.props;

/**
 * Какая-то волшебная каропка
*/
public interface MagicBox {
    String getContents();
}
