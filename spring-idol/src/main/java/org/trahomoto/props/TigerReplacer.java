package org.trahomoto.props;

import org.springframework.beans.factory.support.MethodReplacer;

import java.lang.reflect.Method;

/**
 * Используется для замены реализации метода
 */
public class TigerReplacer implements MethodReplacer {

    @Override
    public Object reimplement(Object o, Method method, Object[] objects) throws Throwable {
        return "Жоский тигра";
    }
}
