package org.trahomoto.members;

import org.trahomoto.PerformanceException;
import org.trahomoto.props.MagicBox;

/**
 * <h1>Иллюзионист</h1>
 */
public class Magician implements Performer {

    public Magician(){}

    @Override
    public void perform() throws PerformanceException {
        System.out.println(magicWords);
        System.out.println("Внутри ящика ...");
        System.out.println(magicBox.getContents());
    }

    private MagicBox magicBox;
    public void setMagicBox(MagicBox magicBox){
        this.magicBox = magicBox;
    }

    private String magicWords;
    public void  setMagicWords(String magicWords){
        this.magicWords = magicWords;
    }
}
