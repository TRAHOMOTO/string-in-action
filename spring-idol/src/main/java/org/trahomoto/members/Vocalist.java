package org.trahomoto.members;

import org.trahomoto.PerformanceException;
import org.trahomoto.instruments.Instrument;

public class Vocalist implements Performer {

    public Vocalist() {}

    @Override
    public void perform() throws PerformanceException {
        System.out.println("Sing: " + song );
    }


    private String song;

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public String screamSong() {
        return song;
    }

}
