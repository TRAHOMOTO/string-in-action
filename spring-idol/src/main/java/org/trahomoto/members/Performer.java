package org.trahomoto.members;

import org.trahomoto.PerformanceException;

public interface Performer {
    public void perform() throws PerformanceException;
}
