package org.trahomoto.members;

import org.trahomoto.PerformanceException;
import org.trahomoto.instruments.Instrument;

/**
 * Другой вариант Исполнителя
 */
public abstract class AbstractInstrumentalist implements Performer {

    private String song;

    public AbstractInstrumentalist(String song){
        this.song = song;
    }

    @Override
    public void perform() throws PerformanceException {
        System.out.print("Поет песню: " + song + " : ");
        getInstrument().play();
    }

    public abstract Instrument getInstrument(); // Внедряемый метод
}
