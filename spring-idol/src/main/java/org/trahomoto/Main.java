package org.trahomoto;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.trahomoto.members.Performer;

public class Main {
    public static void main(String[] args) throws PerformanceException {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("app-context-xml.xml");

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Performer performer1 = (Performer) ctx.getBean("duke");
        performer1.perform();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Performer performer2 = (Performer) ctx.getBean("kenny");
        performer2.perform();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Performer performer3 = (Performer) ctx.getBean("hank");
        performer3.perform();


        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Performer performer4 = (Performer) ctx.getBean("lionSwitch");
        performer4.perform();
        Performer performer5 = (Performer) ctx.getBean("skazi");
        performer5.perform();
        Performer performer6 = (Performer) ctx.getBean("igorNikolaev");
        performer6.perform();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Performer performer7 = (Performer) ctx.getBean("taylor");
        performer7.perform();
        Performer performer8 = (Performer) ctx.getBean("stevie");
        performer8.perform();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Performer performer9 = (Performer) ctx.getBean("harry");
        performer9.perform();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Performer performer10 = (Performer) ctx.getBean("misha");
        performer10.perform();

//        Stage scene = ctx.getBean("theStage", Stage.class);
//        System.out.println(scene);

//        Auditorium auditorium = ctx.getBean("auditorium", Auditorium.class);
//        auditorium= null;

    }
}
