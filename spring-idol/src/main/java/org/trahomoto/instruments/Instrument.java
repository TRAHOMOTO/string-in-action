package org.trahomoto.instruments;

public interface Instrument {
    void play();
}
